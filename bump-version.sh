#!/bin/sh
set -e

VERSION=$1
sed -i.bak -E 's/@[0-9.]+/@'$VERSION'/g' Dockerfile
rm Dockerfile.bak
git add Dockerfile
git commit -m "Bump to $VERSION"
git tag -a v$VERSION -m "standard-version $VERSION"