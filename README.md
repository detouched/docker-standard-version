# standard-version #

This is a Docker image of [standard-version](https://github.com/conventional-changelog/standard-version),
a tool which automates versioning and CHANGELOG generation, with [semantic versioning](https://semver.org/)
and [conventional commit messages](https://www.conventionalcommits.org/).

### Usage ###

Git credentials are required so that `standard-version` can create a commit and tag. These can be passed
as environment variables. Call `standard-version` like this:

```
docker run --rm -it -v $(pwd):/app -e "GIT_AUTHOR_NAME=<your name>" -e "EMAIL=<your email>" standard-version:latest
```

All arguments are passed down to `standard-version` executable.
